'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const businessImagesSchema = new Schema({
  image: {data: Buffer, contentType: String}
},
{
  timestamps: true
},
{
  collection: 'business_images'
})
businessImagesSchema.pre('save', async function save (next) {
})
module.exports = mongoose.model('BusinessImages', businessImagesSchema)
