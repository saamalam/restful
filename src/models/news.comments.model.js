'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const newsCommentSchema = new Schema({
  title: {type: String, required: true, unique: true},
  description: {type: String, required: true},
  user: {type: Schema.Types.ObjectId, ref: 'User'},
  status: {type: Boolean}
},
{
  timestamps: true
},
{
  collection: 'news_comment'
})
newsCommentSchema.pre('save', async function save (next) {
})
module.exports = mongoose.model('NewsComments', newsCommentSchema)
