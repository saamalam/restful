'use strict'
const mongoose = require('mongoose')
const slug = require('mongoose-slug-generator')
const Schema = mongoose.Schema
mongoose.plugin(slug)

const productSchema = new Schema({
  title: {type: String, required: true, unique: true},
  slug: {type: String, slug: 'title', unique: true},
  slogan: {type: String},
  model: {type: String, required: true, unique: true},
  sku: {type: String, unique: true},
  brand: [{type: Schema.Types.ObjectId, ref: 'Business'}],
  description: {type: String, required: true},
  price: {type: Schema.Types.Decimal128, required: true},
  strain: {type: Schema.Types.ObjectId, ref: 'Strain'},
  thc: {type: Number},
  cbd: {type: Number},
  specialPrice: {type: Schema.Types.Decimal128},
  image: {data: Buffer, contentType: String},
  images: [{type: Schema.Types.ObjectId, ref: 'ProductImages'}],
  relatedProduct: [{data: Schema.Types.ObjectId, ref: 'Product'}],
  category: [{type: Schema.Types.ObjectId, require: true, ref: 'Category'}],
  rating: [{type: Schema.Types.ObjectId, ref: 'Rating'}],
  status: {type: Boolean}
},
{
  timestamps: true
})
productSchema.pre('save', async function save (next) {
})
module.exports = mongoose.model('Products', productSchema)
