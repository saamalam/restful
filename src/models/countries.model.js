'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const countrySchema = new Schema({
  countryName: {type: String, unique: true},
  countryShortcode: {type: String, unique: true},
  regions: [{name: String, shortCode: String}]
})

module.exports = mongoose.model('Countries', countrySchema)
