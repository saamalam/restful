'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const strainReviewSchema = new Schema({
  title: {type: String, required: true, unique: true},
  rating: {type: Number, min: 1, max: 5, required: true},
  form: {type: [String], enum: ['Flower', 'Concentrate', 'Edible']},
  consumption: {type: [String], enum: ['Smoke', 'Vaporize']},
  effects: {type: [String], enum: ['Aroused', 'Creative', 'Energetic', 'Euphoric', 'Focused', 'Giggly', 'Happy', 'Hungry', 'Relaxed', 'Sleepy', 'Talkative', 'Tingly', 'Uplifted', 'Anxious', 'Dizzy', 'Dry Eyes', 'Dry Mouth', 'Headache', 'Paranoid']},
  symptoms: {type: [String], enum: ['Cramps', 'Depression', 'Eye Pressure', 'Fatigue', 'Headaches', 'Inflammation', 'Insomnia', 'Lack of Appetite', 'Muscle Spasms', 'Nausea', 'Pain', 'Seizures', 'Spasticity', 'Anxious', 'Stress']},
  flavor: {type: [String], enum: ['Ammonia', 'Apple', 'Apricot', 'Berry', 'Blue Cheese', 'Blueberry', 'Butter', 'Cheese', 'Chemical', 'Chestnut', 'Citrus', 'Coffee', 'Diesel', 'Earthy', 'Flowery', 'Grape', 'Grapefruit', 'Honey', 'Lavender', 'Lemon', 'Lime', 'Mango', 'Menthol', 'Mint', 'Nutty', 'Orange', 'Peach', 'Pear', 'Pepper', 'Pine', 'Pineapple', 'Plum', 'Pungent', 'Rose', 'Sage', 'Skunk', 'Spicy', 'Herbal', 'Strawberry', 'Sweet', 'Tar', 'Tea', 'Tobacco', 'Tree Fruit', 'Tropical', 'Vanilla', 'Violet', 'Woody']},
  description: {type: String, required: true},
  fromDispensary: {type: Schema.Types.ObjectId, ref: 'Business'},
  user: {type: Schema.Types.ObjectId, ref: 'User'},
  isPrivate: {type: Boolean},
  status: {type: Boolean}
},
{
  timestamps: true
},
{
  collection: 'strains_review'
})
strainReviewSchema.pre('save', async function save (next) {
})
module.exports = mongoose.model('strainReview', strainReviewSchema)
