'use strict'
const mongoose = require('mongoose')
const slug = require('mongoose-slug-generator')
const Schema = mongoose.Schema
mongoose.plugin(slug)

const strainSchema = new Schema({
  title: {type: String, required: true, unique: true},
  slug: {type: String, slug: 'title', unique: true},
  shortName: {type: String, required: true},
  slogan: {type: String},
  brand: [{type: Schema.Types.ObjectId, ref: 'Business'}],
  description: {type: String, required: true},
  type: {type: String, default: 'Hybrid', enum: ['Indica', 'Sativa', 'Hybrid']},
  thc: {type: Number},
  cbd: {type: Number},
  effects: [{happy: Number, relaxed: Number, euphoric: Number, uplifted: Number, creative: Number}],
  medical: [{stress: Number, depression: Number, pain: Number, lackOfAppetite: Number, insomnia: Number}],
  negatives: [{dryMouth: Number, dryEyes: Number, anxious: Number, dizzy: Number, paranoid: Number}],
  image: {data: Buffer, contentType: String},
  images: [{type: Schema.Types.ObjectId, ref: 'StrainImages'}],
  relatedStrains: [{data: Schema.Types.ObjectId, ref: 'Strain'}],
  review: [{type: Schema.Types.ObjectId, ref: 'strainReview'}],
  status: {type: Boolean}
},
{
  timestamps: true
})
strainSchema.pre('save', async function save (next) {
})
module.exports = mongoose.model('Strains', strainSchema)
