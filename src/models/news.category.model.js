'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const NewsCategorySchema = new Schema({
  title: {type: String, required: true, unique: true},
  description: {type: String, required: true},
  image: {data: Buffer, contentType: String},
  subCategory: [{type: Schema.Types.ObjectId, ref: 'NewsCategory'}],
  is_parent: {type: Boolean},
  status: {type: Boolean, default: true}
},
{
  timestamps: true
},
{
  collection: 'news_category'
})
NewsCategorySchema.pre('save', async function save (next) {
})
module.exports = mongoose.model('NewsCategory', NewsCategorySchema)
