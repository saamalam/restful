'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ratingSchema = new Schema({
  title: {type: String, required: true, unique: true},
  rating: {type: Number, min: 1, max: 5, required: true},
  description: {type: String, required: true},
  user: {type: Schema.Types.ObjectId, ref: 'User'},
  status: {type: Boolean}
},
{
  timestamps: true
},
{
  collection: 'products_rating'
})
ratingSchema.pre('save', async function save (next) {
})
module.exports = mongoose.model('Rating', ratingSchema)
