'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productImagesSchema = new Schema({
  image: {data: Buffer, contentType: String}
},
{
  timestamps: true
},
{
  collection: 'products_images'
})
productImagesSchema.pre('save', async function save (next) {
})
module.exports = mongoose.model('ProductImages', productImagesSchema)
