'use strict'
const mongoose = require('mongoose')
const slug = require('mongoose-slug-generator')
const Schema = mongoose.Schema
mongoose.plugin(slug)

const newsSchema = new Schema({
  title: {type: String, required: true, unique: true},
  slug: {type: String, slug: 'title', unique: true},
  description: {type: String, required: true},
  metaDescription: {type: String},
  metaKeywords: {type: String},
  comments: [{type: Schema.Types.ObjectId, ref: 'NewsComments'}],
  image: {data: Buffer, contentType: String},
  relatedNews: [{data: Schema.Types.ObjectId, ref: 'News'}],
  category: [{type: Schema.Types.ObjectId, require: true, ref: 'NewsCategory'}],
  author: {type: Schema.Types.ObjectId, ref: 'User'},
  tags: {type: String},
  status: {type: Boolean, default: 0, enum: [0, 1]}
},
{
  timestamps: true
})
newsSchema.pre('save', async function save (next) {
})
module.exports = mongoose.model('News', newsSchema)
