'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const businessReviewSchema = new Schema({
  title: {type: String, required: true, unique: true},
  quality: {type: Number, min: 1, max: 5, required: true},
  service: {type: Number, min: 1, max: 5, required: true},
  atmosphere: {type: Number, min: 1, max: 5, required: true},
  staff: {type: Number, min: 1, max: 5, required: true},
  facilities: {type: Number, min: 1, max: 5, required: true},
  description: {type: String, required: true},
  user: {type: Schema.Types.ObjectId, ref: 'User'},
  status: {type: Boolean}
},
{
  timestamps: true
},
{
  collection: 'business_review'
})
businessReviewSchema.pre('save', async function save (next) {
})
module.exports = mongoose.model('BusinessReview', businessReviewSchema)
