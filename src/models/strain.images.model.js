'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const strainImagesSchema = new Schema({
  image: {data: Buffer, contentType: String}
},
{
  timestamps: true
},
{
  collection: 'strains_images'
})
strainImagesSchema.pre('save', async function save (next) {
})
module.exports = mongoose.model('StrainImages', strainImagesSchema)
