'use strict'
const mongoose = require('mongoose')
const slug = require('mongoose-slug-generator')
const Schema = mongoose.Schema
mongoose.plugin(slug)

const businessSchema = new Schema({
  title: {type: String, required: true, unique: true},
  slug: {type: String, slug: 'title', unique: true},
  description: {type: String, required: true},
  email: {type: String},
  phone: {type: String},
  website: {type: String},
  address: {type: String},
  city: {type: String},
  zip: {type: Number},
  country: {type: Schema.Types.ObjectId, ref: 'Country'},
  state: {type: String},
  location: {type: {type: String, enum: ['Point'], required: true}, coordinates: {type: [Number], required: true}},
  news: [{type: Schema.Types.ObjectId, ref: 'News'}],
  deals: [{type: Schema.Types.ObjectId, ref: 'Deals'}],
  strains: [{type: Schema.Types.ObjectId, ref: 'Strain'}],
  products: [{type: Schema.Types.ObjectId, ref: 'Product'}],
  type: {type: [String], enum: ['Dispensary', 'Recreational', 'Doctor', 'Delivery', 'Brand', 'Producer']},
  following: [{type: Schema.Types.ObjectId, ref: 'User'}],
  review: [{type: Schema.Types.ObjectId, ref: 'BusinessReview'}],
  operationHours: [{type: String}],
  image: {data: Buffer, contentType: String},
  gallery: [{type: Schema.Types.ObjectId, ref: 'BusinessImages'}],
  socialLinks: {type: Map, of: String},
  status: {type: Boolean, default: 0, enum: [0, 1]},
  verified: {type: Boolean}
},
{
  timestamps: true
})
businessSchema.pre('save', async function save (next) {
})
module.exports = mongoose.model('Business', businessSchema)
