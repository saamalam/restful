'use strict'

const config = require('../config')
const express = require('express')
const http = require('http')
const morgan = require('morgan')
const session = require('express-session')
const cors = require('cors')
const socketio = require('socket.io')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const errorHandler = require('../middlewares/error-handler')
const apiRouter = require('../routes/api')
const passport = require('passport')
const passportJwt = require('../services/passport')

const app = express()
app.use(bodyParser.json())
app.use(cors())
app.use(helmet())

const server = http.createServer(app);

if (config.env !== 'test') app.use(morgan('combined'))

// passport
app.use(passport.initialize())
passport.use('jwt', passportJwt.jwt)

// saveUninitialized: true allows us to attach the socket id to the session
// before we have athenticated the user
app.use(session({
  secret: process.env.SESSION_SECRET,
  resave: true,
  saveUninitialized: true
}))

// Connecting sockets to the server and adding them to the request
// so that we can access them later in the controller
// eslint-disable-next-line no-undef
const io = socketio(server)
app.set('io', io)

app.use('/api', apiRouter)
app.use(errorHandler.handleNotFound)
app.use(errorHandler.handleError)

exports.start = () => {
  app.listen(config.port, (err) => {
    if (err) {
      console.log(`Error : ${err}`)
      process.exit(-1)
    }

    console.log(`${config.app} is running on ${config.port}`)
  })
}

exports.app = app
