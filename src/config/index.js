require('dotenv').config() // load .env file

module.exports = {
  port: process.env.PORT,
  app: process.env.APP,
  env: process.env.NODE_ENV,
  secret: process.env.APP_SECRET,
  session_secret: process.env.SESSION_SECRET,
  mongo: {
    uri: process.env.MONGOURI,
    testURI: process.env.MONGOTESTURI
  },
  fbClientId: process.env.FB_CLIENT_ID,
  fbClientSecret: process.env.FB_CLIENT_SECRET,
  fbCallbackURL: process.env.FB_CALLBACK_URL,
  gClientId: process.env.G_CLIENT_ID,
  gClientSecret: process.env.G_CLIENT_SECRET,
  gCallbackURL: process.env.G_CALLBACK_URL

}
