const passport = require('passport')
const GoogleStrategy = require('passport-google-oauth2').Strategy
const config = require('../config')
const User = require('../models/user.model')
// Serialize user into the sessions
passport.serializeUser(function (user, done) {
  done(null, user)
})
// Deserialize user from the sessions
passport.deserializeUser(function (user, done) {
  done(null, user)
})

passport.use(new GoogleStrategy({
  clientID: config.gClientId,
  clientSecret: config.gClientSecret,
  callbackURL: config.gCallbackURL,
  profileFields: ['emails'],
  passReqToCallback: true
}, async function (request, accessToken, refreshToken, profile, done) {
  User.findOrCreate({email: profile.emails[0].value}, {email: profile.emails[0].value, name: profile.displayName, username: profile.emails[0].value, password: profile.id, gender: profile.gender || 'others'}, function (err, user) {
    if (err) { return done(err) }
    done(null, user)
  })
}
))

module.exports = passport
