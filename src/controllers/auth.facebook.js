const passport = require('passport')
const FacebookStrategy = require('passport-facebook').Strategy
const config = require('../config')
const User = require('../models/user.model')
const bcrypt = require('bcrypt-nodejs')
// Serialize user into the sessions
passport.serializeUser(function (user, done) {
  done(null, user)
})
// Deserialize user from the sessions
passport.deserializeUser(function (user, done) {
  done(null, user)
})

passport.use(new FacebookStrategy({
  clientID: config.fbClientId,
  clientSecret: config.fbClientSecret,
  callbackURL: config.fbCallbackURL,
  profileFields: ['id', 'emails', 'name', 'gender', 'photos']
}, async (accessToken, refreshToken, profile, done) => {
  User.findOrCreate({email: profile.emails[0].value}, {email: profile.emails[0].value, name: profile.displayName, username: profile.emails[0].value, password: profile.id, gender: profile.gender || 'others'}, function (err, user) {
    if (err) { return done(err) }
    done(null, user)
  })
}
))

module.exports = passport
