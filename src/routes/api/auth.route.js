'use strict'

const express = require('express')
const router = express.Router()
const authController = require('../../controllers/auth.controller')
const authFacebook = require('../../controllers/auth.facebook')
const authGoogle = require('../../controllers/auth.google')
const validator = require('express-validation')
const { create } = require('../../validations/user.validation')
const auth = require('../../middlewares/authorization')
const User = require('../../models/user.model')
const jwt = require('jsonwebtoken')
const config = require('../../config')

router.post('/register', validator(create), authController.register) // validate and register
router.post('/login', authController.login) // login
router.post('/authenticate', authController.login) // sudo authentication & login

// This custom middleware allows us to attach the socket id to the session.
// With the socket id attached we can send back the right user info to
// the right socket
const addSocketIdtoSession = (req, res, next) => {
  req.session.socketId = req.query.socketId
  next()
}

// Social Login
router.get('/facebook', addSocketIdtoSession, authFacebook.authenticate('facebook', { scope: 'email' }))
router.get('/facebook/callback', authFacebook.authenticate('facebook', { failureRedirect: '/login' }),
  async function (req, res, next) {
    const io = req.app.get('io')
    io.in(req.session.socketId).emit('google', req.user)
    try {
      const user = await User.generateSocialToken(req.user)
      const payload = {sub: user.id}
      const token = jwt.sign(payload, config.secret)
      return res.json({ message: 'OK', token: token })
    } catch (error) {
      next(error)
    }
  })
router.get('/google', addSocketIdtoSession, authGoogle.authenticate('google', { scope: ['profile', 'email'] }))

router.get('/google/callback', authGoogle.authenticate('google', { failureRedirect: '/login' }),
  async function (req, res, next) {
    const io = req.app.get('io')
    io.in(req.session.socketId).emit('google', req.user)
    try {
      const user = await User.generateSocialToken(req.user)
      const payload = {sub: user.id}
      const token = jwt.sign(payload, config.secret)
      return res.json({ message: 'OK', token: token })
    } catch (error) {
      next(error)
    }
  }
)

// Authentication example
router.get('/secret1', auth(), (req, res) => {
  // example route for auth
  res.json({ message: 'Anyone can access(only authorized)' })
})
router.get('/secret2', auth(['admin']), (req, res) => {
  // example route for auth
  res.json({ message: 'Only admin can access' })
})
router.get('/secret3', auth(['user']), (req, res) => {
  // example route for auth
  res.json({ message: 'Only user can access' })
})

module.exports = router
