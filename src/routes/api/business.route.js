'use strict'

const express = require('express')
const router = express.Router()
const Business = require('../../models/business.model')
const businessImages = require('../../models/business.images.model')
const businessReview = require('../../models/business.review.model')

router.get('/business', (req, res) => {
  // eslint-disable-next-line handle-callback-err
  Business.find((err, business) => {
    if (err) console.log(err)
    res.json({business: business})
  })
})

router.get('/business/:id', (req, res) => {
  // eslint-disable-next-line handle-callback-err
  Business.find((err, business) => {
    if (err) console.log(err)
    res.json({business: business})
  })
})

module.exports = router
