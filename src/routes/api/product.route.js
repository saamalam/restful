'use strict'

const express = require('express')
const router = express.Router()
const Product = require('../../models/product.model')
const productImages = require('../../models/product.images.model')
const productRating = require('../../models/product.rating.model')

router.get('/products', (req, res) => {
  // eslint-disable-next-line handle-callback-err
  Product.find((err, product) => {
    if (err) console.log(err)
    res.json({product: product})
  })
})

module.exports = router
