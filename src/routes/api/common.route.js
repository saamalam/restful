'use strict'

const express = require('express')
const router = express.Router()
const Countries = require('../../models/countries.model')

router.get('/countries', (req, res) => {
  // eslint-disable-next-line handle-callback-err
  Countries.find((err, countries) => {
    if (err) console.log(err)
    res.json({countries: countries})
  })
})

module.exports = router
