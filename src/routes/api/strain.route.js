'use strict'

const express = require('express')
const router = express.Router()
const Strain = require('../../models/strain.model')
const strainImages = require('../../models/strain.images.model')
const strainReview = require('../../models/strain.review.model')

router.get('/strains', (req, res) => {
  // eslint-disable-next-line handle-callback-err
  Strain.find((err, strain) => {
    if (err) console.log(err)
    res.json({strain: strain})
  })
})

module.exports = router
